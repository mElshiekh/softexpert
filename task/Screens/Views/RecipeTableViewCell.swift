//
//  RecipeTableViewCell.swift
//  task
//
//  Created by Mac Admin on 10/25/19.
//  Copyright © 2019 shiekh. All rights reserved.
//

import UIKit
import Kingfisher

class RecipeTableViewCell: UITableViewCell {
    
    static let identifier = "RecipeTableViewCell"
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var link: UILabel!
    @IBOutlet weak var healthLabels: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setup(item: Recipe) {
        let url = URL(string: item.image ?? "")
        DispatchQueue.main.async {[unowned self] in
            self.mainImage.kf.setImage(with: url)
        }
        title.text = item.label
        link.text = item.url
        healthLabels.text = item.healthLabelsString
    }

}
