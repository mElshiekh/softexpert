//
//  DetailsViewController.swift
//  task
//
//  Created by Mac Admin on 10/25/19.
//  Copyright © 2019 shiekh. All rights reserved.
//

import UIKit
import SafariServices

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var linkLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var recipe: Recipe!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        let url = URL(string: recipe.image ?? "")
        DispatchQueue.main.async {[unowned self] in
            self.mainImage.kf.setImage(with: url)
        }
        titleLabel.text = recipe.label
        linkLabel.text = recipe.url
        linkLabel.isUserInteractionEnabled = true
        linkLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openLink)))
    }
    
    @objc func openLink(){
        let url = URL(string: recipe.url ?? "")
        if let _ = url{
            let webVC = SFSafariViewController(url: url!)
            present(webVC, animated: true, completion: nil)
        }
    }
    
}

extension DetailsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipe.ingredientLines?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ingredientCell")!
        cell.textLabel?.text = recipe.ingredientLines?[indexPath.row]
        return cell
    }
    
}
