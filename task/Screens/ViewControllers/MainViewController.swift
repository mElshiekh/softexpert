//
//  MainViewController.swift
//  task
//
//  Created by Mac Admin on 10/24/19.
//  Copyright © 2019 shiekh. All rights reserved.
//

import UIKit
import RxSwift

class MainViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchHistoryView: UIView!
    @IBOutlet weak var searchHistoryTableView: UITableView!
    
    var dataSource = SearchHistoryDataSource()
    
    var searchQuery = ""
    
    var recipes:[Recipe]{
        return viewModel.viewData.value ?? []
    }
    var viewModel = MainScreenViewModel()
    var disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 150
        searchTextField.becomeFirstResponder()
        searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        setupSearchDataSource()
        registerCells()
        setupBinding()
    }
    
    func setupSearchDataSource(){
        dataSource.setup(tableView: searchHistoryTableView, delegate: self)
    }
    
    @objc func textFieldDidChange(_ sender: UITextField){
        if (sender.text ?? "") != ""{
            if searchHistoryView.isHidden{
                searchHistoryView.isHidden = false
                dataSource.update(isShow: true)
            }
        }
        else{
            searchHistoryView.isHidden = true
        }
    }
    
    func registerCells(){
        tableView.register(UINib(nibName: RecipeTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: RecipeTableViewCell.identifier)
    }
    
    func setupBinding(){
        viewModel.viewData.asObservable().subscribe(onNext: { [unowned self] viewData in
            if viewData != nil && viewData!.count > 0{
                self.tableView.reloadData()
            }
        }).disposed(by: disposeBag)
    }
    
    @IBAction func SearchAction(_ sender: Any) {
        if searchTextField.text ?? "" != ""{
            search(query: searchTextField.text!)
        }
    }
    
    func search(query:String){
        searchHistoryView.isHidden = true
        searchQuery = query
        viewModel.getRecipes(query: query, reset: true)
        UserDefaultsHelper.addToSearchHistory(value: query)
    }
    
    
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RecipeTableViewCell.identifier) as! RecipeTableViewCell
        cell.setup(item: recipes[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (recipes.count - 1) && viewModel.pageCount == (recipes.count/viewModel.itemsPerPage){
            viewModel.getRecipes(query: searchQuery)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let storyBoard = UIStoryboard(name: "Details", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "DetailesScreen") as! DetailsViewController
        vc.recipe = recipes[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

extension MainViewController: SearchHistoryDelegate{
    
    func setSelected(value: String) {
        searchTextField.text = value
        search(query: value)
    }
}
