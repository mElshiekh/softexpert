//
//  UserDefaultsHelper.swift
//  task
//
//  Created by Mac Admin on 10/25/19.
//  Copyright © 2019 shiekh. All rights reserved.
//

import Foundation


class UserDefaultsHelper {
    class func addToSearchHistory(value: String){
        var values = getSearchHistory()
        for item in values {
            if item == value{
                return
            }
        }
        if values.count > 10{
            values.removeFirst()
        }
        values.append(value)
        UserDefaults.standard.set(values, forKey: Constants.searchHistory)
    }
    
    class func getSearchHistory() -> [String]{
        return UserDefaults.standard.array(forKey: Constants.searchHistory) as? [String] ?? []
    }
}
