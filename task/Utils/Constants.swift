//
//  Constants.swift
//  task
//
//  Created by Mac Admin on 10/22/19.
//  Copyright © 2019 shiekh. All rights reserved.
//

import Foundation

class Constants{
    static var appID = Bundle.main.object(forInfoDictionaryKey: "appID") as? String ?? "" //to be changed
    static var appKey = Bundle.main.object(forInfoDictionaryKey: "appKey") as? String ?? "" //to be changed
    static let searchHistory = "searchHistory"
}
