//
//  NetworkUtil.swift
//  task
//
//  Created by Mac Admin on 10/22/19.
//  Copyright © 2019 shiekh. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

enum EndPointUrls:String {
    case search = "search?app_id=%@&app_key=%@&q=%@&from=%@"
}
enum BaseUrls:String {
    case base = "https://api.edamam.com/"
}

enum NetworkError {
    case NoInternet
    case CouldNotParseJson
    case ServerError
}

class NetworkManager{
    
    func isInternetAvailable() -> Bool {
        return (NetworkReachabilityManager()?.isReachable)!
    }
    
    static var shared: NetworkManager = NetworkManager()
    
    private init(){}
    
    
    func processReq<T>(url:String, returnType: T.Type, headers:HTTPHeaders? = nil) -> Observable<T?> where T :  Decodable {
        let ret = PublishSubject<T?>()
        if isInternetAvailable(){
            Alamofire.request(url, method: .get, parameters: nil, headers: headers).validate().responseJSON { [unowned self] response  in
                switch response.result {
                case .success:
                    self.parseResponse(data: response.data, objResponse: ret)
                case .failure :
                    self.showError(error: .ServerError)
                }
            }
        }else{
            showError(error: .NoInternet)
        }
        return ret
    }
    
    func showError(error:NetworkError){
        
    }
    
    func parseResponse<T>(data:Data?,objResponse:PublishSubject<T>)  where T : Decodable {
        if let _ = data{
            do{
                let response = try JSONDecoder().decode(T.self, from: data!)
                objResponse.onNext(response)
                objResponse.onCompleted()
            }
            catch {
                showError(error: .ServerError)
            }
        }
    }
    
    
}
