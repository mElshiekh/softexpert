//
//  SearchDataSource.swift
//  task
//
//  Created by Mac Admin on 10/25/19.
//  Copyright © 2019 shiekh. All rights reserved.
//

import UIKit

protocol SearchHistoryDelegate: class {
    func setSelected(value: String)
}

class SearchHistoryDataSource: NSObject {
    var tableView: UITableView!
    weak var delegate: SearchHistoryDelegate!
    var items: [String] = []
    
    func setup(tableView: UITableView, delegate: SearchHistoryDelegate) {
        self.tableView = tableView
        tableView.delegate = self
        tableView.dataSource = self
        self.delegate = delegate
    }
    
    func update(isShow: Bool){
        items = UserDefaultsHelper.getSearchHistory()
        tableView.reloadData()
    }
}

extension SearchHistoryDataSource: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell")!
        cell.textLabel?.text = items[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate.setSelected(value: items[indexPath.row])
    }
    
}
