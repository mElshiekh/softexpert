//
//  ServiceType.swift
//  task
//
//  Created by Mac Admin on 10/24/19.
//  Copyright © 2019 shiekh. All rights reserved.
//

import Foundation

class ServiceType {
    var networkManager = NetworkManager.shared
    func getFullUrl(baseUrl:BaseUrls,endPoint:EndPointUrls) -> String{
        return "\(baseUrl.rawValue)\(endPoint.rawValue)"
    }
    func getFullUrlwithFormat(baseUrl:BaseUrls,endPoint:EndPointUrls,args:[String]) -> String{
        let url = "\(baseUrl.rawValue)\(endPoint.rawValue)"
        let fullUrl = String(format: url, arguments: args)
        return fullUrl
    }
}
