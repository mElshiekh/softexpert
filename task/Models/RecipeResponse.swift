//
//  RecipeResponse.swift
//  task
//
//  Created by Mac Admin on 10/25/19.
//  Copyright © 2019 shiekh. All rights reserved.
//

import Foundation
class RecipeResponse: Decodable {
    var q : String?
    var from : Int?
    var to : Int?
    var params : Params?
    var more : Bool?
    var count : Int?
    var hits : [Hits]?
}

class Params: Decodable {
    var sane : [String]?
    var q : [String]?
    var app_app_key : [String]?
    var app_id : [String]?
}

class Hits: Decodable {
    var recipe : Recipe?
    var bookmarked : Bool?
    var bought : Bool?
}

class Recipe: Decodable {
    var uri : String?
    var label : String?
    var image : String?
    var source : String?
    var url : String?
    var shareAs : String?
    var healthLabels : [String]?
    var ingredientLines : [String]?
    
    var healthLabelsString:String{
        return healthLabels?.joined(separator: ", ") ?? ""
    }
}
