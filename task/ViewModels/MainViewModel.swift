//
//  MainViewModel.swift
//  task
//
//  Created by Mac Admin on 10/25/19.
//  Copyright © 2019 shiekh. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class MainScreenViewModel{
    var disposeBag = DisposeBag()
    var apiService = SearchRecipeApi()
    var viewData = BehaviorRelay<[Recipe]?>(value: [])
    let itemsPerPage = 10
    
    var pageCount = 0
    
    func getRecipes(query: String, reset: Bool = false){
        if reset{
            pageCount = 0
        }
        self.pageCount += 1
        apiService.searchRecipes(startIndex: pageCount*itemsPerPage, query: query).asObservable().subscribe(onNext: { [unowned self] response in
            if reset{
                self.viewData.accept([])
            }
            let newArray = response?.hits?.map({ (hit) -> Recipe in
                return hit.recipe!
            })
            self.viewData.accept((self.viewData.value ?? []) + (newArray ?? []))
        }).disposed(by: disposeBag)
    }
}
