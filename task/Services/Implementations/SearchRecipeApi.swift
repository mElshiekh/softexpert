//
//  SearchRecipeApi.swift
//  task
//
//  Created by Mac Admin on 10/25/19.
//  Copyright © 2019 shiekh. All rights reserved.
//

import Foundation
import RxSwift

class SearchRecipeApi:ServiceType, SearchRecipesProtocol {
    func searchRecipes(startIndex: Int, query: String) -> Observable<RecipeResponse?> {
        let url = getFullUrlwithFormat(baseUrl: BaseUrls.base, endPoint: EndPointUrls.search, args:[Constants.appID, Constants.appKey, query, "\(startIndex)"])
        return networkManager.processReq(url: url, returnType: RecipeResponse.self)
    }
    
    
}
