//
//  ServiceProtocols.swift
//  task
//
//  Created by Mac Admin on 10/24/19.
//  Copyright © 2019 shiekh. All rights reserved.
//

import Foundation
import RxSwift

protocol SearchRecipesProtocol{
    func searchRecipes(startIndex:Int, query: String) -> Observable<RecipeResponse?>
}
